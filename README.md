# foreverfaster

Web application for a sports car detailing company.  Built with Python Flask framework, Bulma CSS framework, Google Fonts, custom generated web icons based on Fontastic 4, HTML5, CSS, and JavaScript.